const jwt = require("jsonwebtoken");

const verify = (req, res, next) => {
    const authHeader = req.headers.auth;
    if (authHeader) {
        const token = authHeader.split(" ")[1];
        jwt.verify(token, "secretKey", (err, user) => {
            if (err) {
                return res.status(401).json("invalid token")
            }
            req.user = user;
            next();
        })
    } else {
        res.status(401).json("not authenticated")
    }
}
module.exports = {verify}