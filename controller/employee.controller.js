const Employee = require("../models/employee.model");
class EmployeeController {
    async getAllEmployees(req, res) {
        try {
            const data = await Employee.find()
            res.json({
                status: true,
                message: "Data Loaded",
                data: data
            })
        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded",
            })
        }
    }
    async getEmployeeById(req, res) {
        const { id } = req.params;
        try {
            const data = await Employee.findById(id)
            res.json({
                status: true,
                message: "Data Loaded",
                data: data
            })

        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded",
               
            })
        }
    }
    async createEmployee(req, res) {
        const { name, surname, contact,jobDescription, salary,startDate,endDate } = req.body
        const newEmployee = new Employee({
            name: name,
            surname: surname,
contact:contact,
endDate:endDate,
            jobDescription: jobDescription,
            salary: salary,
            contactNumber: contact,
            startDate: startDate
        })
        try {
    const saveData = await newEmployee.save()
    res.json(saveData)
}
catch (err) {
    res.json({
        status: 500,
        message: "Data Not Loaded",
        data: data
    })
}
    }
    async updateEmployee(req, res) {
    const { id } = req.params;
    const { name, surname, contact,jobDescription, salary,startDate,endDate } = req.body
    try {
        const data = await Employee.updateOne({ _id: id }, {
            $set: {
                name: name,
                surname: surname,
                jobDescription: jobDescription,
contact:contact,
endDate:endDate,
                salary: salary,
                contactNumber: contact,
                startDate: startDate
            }
        })
        res.json({
            status: 200,
            message: "Data Updated",
            data: data
        })

    }
    catch (err) {
        res.json({
            status: 500,
            message: "Data Not Updated",
        })
    }


}
    async deleteEmployee(req, res) {
    const { id } = req.params;
    try {
        const data = await Employee.deleteOne({ _id: id })
        res.json({
            status: true,
            message: " Data successfully deleted",
            data: data
        })
    } catch (err) {
        res.json({
            status: 500,
            message: "Data Not Deleted",
        })
    }
}
}
module.exports = EmployeeController;