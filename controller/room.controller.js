const Room = require("../models/Room.model");
class RoomController {
    async getAllRooms(req, res) {
        try {
            const data = await Room.find()
            res.json({
                status: true,
                message: "Data Loaded",
                data: data
            })
        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded",
                data: data
            })
        }
    }
    async getRoomById(req, res) {
        const { id } = req.params;
        try {
            const data = await Room.findById(id)
            res.json({
                status: true,
                message: "Data Loaded",
                data: data
            })

        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded",
                data: data
            })
        }
    }
    async createRoom(req, res) {
        const { roomNumber, roomKey,roomOwner, roomPrice, dateBuilt, roomDescription, roomType, roomStatus } = req.body
        const newRoom = new Room({
            roomNumber: roomNumber,
            roomKey: roomKey,
            roomPrice: roomPrice,
            roomOwner: roomOwner,
            dateBuilt: dateBuilt,
            roomDescription: roomDescription,
            roomType: roomType,
            roomStatus: roomStatus

        })
        try {
            const data = await newRoom.save()
            res.json({
                status: 500,
                message: "Data Loaded",
                data: data
            })
        }
        catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded"
            })
        }
    }
    async updateRoom(req, res) {
        const { id } = req.params;
        const { roomNumber, roomKey,roomOwner, roomPrice, dateBuilt, roomDescription, roomType, roomStatus } = req.body
        try {
            const data = await Room.updateOne({ _id: id }, {
                $set: {
                    roomNumber: roomNumber,
                    roomKey: roomKey,
                    roomPrice: roomPrice,
                    roomOwner: roomOwner,
                    dateBuilt: dateBuilt,
                    roomDescription: roomDescription,
                    roomType: roomType,
                    roomStatus: roomStatus
                }
            })
            res.json({
                status: 200,
                message: "Data Updated",
                data: data
            })

        }
        catch (err) {
            res.json({
                status: 500,
                message: "Data Not Updated",
            })
        }


    }
    async deleteRoom(req, res) {
        const { id } = req.params;
        try {
            const data = await Room.deleteOne({ _id: id })
            res.json({
                status: true,
                message: " Data successfully deleted",
                data: data
            })
        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Deleted",
            })
        }
    }
}
module.exports = RoomController;