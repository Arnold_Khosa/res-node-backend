const WaitingList = require("../models/waitinglist.model");

class WaitingListController {
    async getAllWaitingLists(req, res) {
        try {
            const data = await WaitingList.find()
            res.json({
                status: true,
                message: "Data Loaded",
                data: data
            })
        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded",
                data: data
            })
        }
    }
    async getWaitingListById(req, res) {
        const { id } = req.params;
        try {
            const data = await WaitingList.findById(id)
            res.json({
                status: true,
                message: "Data Loaded",
                data: data
            })

        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded",
                data: data
            })
        }
    }
    async createWaitingList(req, res) {
        const { name, surname, contact, roomTypeRequested, upfrontPayment, email } = req.body
        const newWaitingList = new WaitingList({
            name: name,
            surname: surname,
            contact: contact,
            email: email,
            roomTypeRequested: roomTypeRequested,
            upfrontPayment: upfrontPayment

        })
        try {
            const saveData = await newWaitingList.save()
            res.json(saveData)
        }
        catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded",
                data: data
            })
        }
    }
    async updateWaitingList(req, res) {
        const { id } = req.params;
        const { name, surname, contact, roomTypeRequested, upfrontPayment, email } = req.body
        try {
            const data = await WaitingList.updateOne({ _id: id }, {
                $set: {
                    name: name,
                    surname: surname,
                    contact: contact,
                    email: email,
                    roomTypeRequested: roomTypeRequested,
                    upfrontPayment: upfrontPayment
                }
            })
            res.json({
                status: 200,
                message: "Data Updated",
                data: data
            })

        }
        catch (err) {
console.log(err)
            res.json({
                status: 500,
                message: "Data Not Updated",
            })
        }


    }
    async deleteWaitingList(req, res) {
        const { id } = req.params;
        try {
            const data = await WaitingList.deleteOne({ _id: id })
            res.json({
                status: true,
                message: " Data successfully deleted",
                data: data
            })
        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Deleted",
            })
        }
    }
}
module.exports = WaitingListController;