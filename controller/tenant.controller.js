const Tenant = require("../models/tenant.model");
class TenantController {
    async getAllTenants(req, res) {
        try {
            const data = await Tenant.find()
            res.json({
                message: "Data Loaded",
                data: data
            })
        } catch (err) {
            res.json({
                message: "Data Not Loaded",
            })
        }
    }
    async getTenantById(req, res) {
        const { id } = req.params;
        try {
            const data = await Tenant.findById(id)
            res.json({
                status: true,
                message: "Data Loaded",
                data: data
            })

        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded",
                data: data
            })
        }
    }
    async createTenant(req, res) {
        const { name, surname, contact, dateMovedIn, duePay, roomNumber } = req.body
        const newTenant = new Tenant({
            name: name,
            surname: surname,
            contact: contact, 
            dateMovedIn: dateMovedIn,
            duePay: duePay,
            roomNumber: roomNumber
        })
        try {
            const saveData = await newTenant.save()
            res.json(saveData)
        }
        catch (err) {
            res.json({
                status: 500,
                message: "Data Not Loaded",
                data: data
            })
        }
    }
    async updateTenant(req, res) {
        const { id } = req.params;
        const { name, surname, contact, dateMovedIn, duePay,roomNumber } = req.body
        try {
            const data = await Tenant.updateOne({ _id: id }, {
                $set: {
                    name: name,
                    surname: surname,
                    contact: contact,
                    dateMovedIn: dateMovedIn,
                    duePay: duePay,
    roomNumber,roomNumber
                }
            })
            res.json({
                status: 200,
                message: "Data Updated",
                data: data
            })

        }
        catch (err) {
            res.json({
                status: 500,
                message: "Data Not Updated",
            })
        }


    }
    async deleteTenant(req, res) {
        const { id } = req.params;
        try {
            const data = await Tenant.deleteOne({ _id: id })
            res.json({
                status: true,
                message: " Data successfully deleted",
                data: data
            })
        } catch (err) {
            res.json({
                status: 500,
                message: "Data Not Deleted",
            })
        }
    }
}
module.exports = TenantController;