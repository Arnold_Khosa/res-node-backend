const User = require("../models/user.model")
const RefreshToken = require("../models/refreshtokens.model")
const jwt = require("jsonwebtoken");

class UserController {
    generateAccessToken(user) {
        return jwt.sign({ id: user._id, username: user.username, isAdmin: user.isAdmin }, "secretKey", { expiresIn: "10h" })
    }
    generateRefreshToken(user) {
        return jwt.sign({ id: user.id, username: user.username, isAdmin: user.isAdmin }, "RefreshTokenSecretKey", { expiresIn: "12h" })
    }
    async login(req, res) {
        const { password, username } = req.body;
        try {
            const user = await User.findOne({
                username: username
            })
            if (user && user.password === password) {

                const accessToken = this.generateAccessToken(user);
                const refreshToken = this.generateRefreshToken(user);
                const newRefreshToken = new RefreshToken({
                    refreshToken: refreshToken
                });
                const saveData = await newRefreshToken.save();
                res.json({
                    id: user._id,
                    username: user.username,
                    isAdmin: user.isAdmin,
                    accessToken, refreshToken
                })
            } else {
                res.status(400).json({message:"password or username do not match"})
            }
        } catch (err) { console.log(err) }
    }
    async logout(req, res) {
        const refreshToken = req.body.Token;
        try {
            const data = await RefreshToken.deleteOne({ refreshToken: refreshToken })
        } catch (err) {
            console.log(err)
        }
        res.status(200).json({ message: "Successflly logged out" })
    }
    async refreshToken(req, res) {
        //refresh Token from user
        try {
        const refreshToken = req.body.token;

        if (!refreshToken) {
            return res.status(403).json({message:"Not authenticated"});
        }
        const tokens = await RefreshToken.find({
            refreshToken: refreshToken
        })
        if (tokens.length === 0) {
            res.status(403).json({message:"Invalid token"})
        }
        console.log(tokens);
       
        
            jwt.verify(refreshToken, "RefreshTokenSecretKey", (err, user) => {

                const Access_Token = this.generateAccessToken(user);
                const Refresh_Token = this.generateRefreshToken(user);
                const newRefreshToken = new RefreshToken({
                    refreshToken: Refresh_Token
                }).save();
                res.json({
                    id: user.id,
                    username: user.username,
                    isAdmin: user.isAdmin,
                    accessToken: Access_Token,
                    refreshToken: Refresh_Token
                })

            })
        } catch (error) {
            res.status(408).json({ message: "Token has expired", timeout: true })
        }

        const del = await RefreshToken.deleteOne({ refreshToken: refreshToken })
    }
    async deleteUser(req, res) {

    }
    async AddUser(req, res) {

    }
    async removeUser(req, res) {

    }
}
module.exports = UserController;