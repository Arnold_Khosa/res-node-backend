const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
require("dotenv").config();
//
const app = express();
//
const tenantRoutes = require("./routes/tenant.routes")
const roomRoutes = require("./routes/room.routes")
const employeeRoutes = require("./routes/employee.routes")
const waitingListRoutes = require("./routes/waitinglist.routes")
const userRoutes = require("./routes/user.routes.js")
//
app.use(cors())
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(bodyParser.raw())
//
const {verify}= require("./middlewares/Authentication/auth")
const UserController= require("./controller/user.controller")
const User_controller = new UserController();
//app.use("/register",userRoutes)
app.use("/login",userRoutes)
app.post("/refresh" ,(req,res)=>{User_controller.refreshToken(req,res)})
app.post("/logout",(req,res)=>{User_controller.logout(req,res)})
app.use("/api/tenants",verify,tenantRoutes)
app.use("/api/rooms",verify, roomRoutes)
app.use("/api/employees",verify, employeeRoutes)
app.use("/api/waitinglist",verify, waitingListRoutes)
//connection to mongo db
mongoose.connect(process.env.ATLAS_URI).then(() => { console.log("connected") })
    .catch((err) => { console.log("Failed", err) })

const port = process.env.port || 5000;
app.listen(port, () => {
    console.log(`server listening at port ${port}`);
})
