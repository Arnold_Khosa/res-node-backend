const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const employeeSchema = new Schema({
    name: {
        type: String
    },
    surname: {
        type: String
    },
    jobDescription: {
        type: String
    },
    salary: {
        type: String
    },
    contact: {
        type: String
    },
    startDate: {
        type: String
    },
endDate: {
        type: String
    }
})
module.exports = mongoose.model("employees",employeeSchema)