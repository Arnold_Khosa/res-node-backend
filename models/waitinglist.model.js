const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const waitingListSchema = new Schema({
    name: {
        type: String
    },
   surname: {
        type: String
    },
    contact: {
        type: String
    },
   email: {
        type: String
    },
    roomTypeRequested: {
        type: String
    },
    upfrontPayment: {
        type: String
    }

})
 
module.exports = mongoose.model("waitinglist",waitingListSchema)