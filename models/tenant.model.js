const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const tenantSchema = new Schema({
    name: {
        type: String
    },
    surname: {
        type: String
    },
    contact: {
        type: String
    },
    dateMovedIn: {
        type: String
    },
    duePay: {
        type: String
    },
  roomNumber:{type: String}
    
})
module.exports = mongoose.model("Tenants",tenantSchema)