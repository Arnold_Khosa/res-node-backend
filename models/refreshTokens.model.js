const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
    refreshToken:{
        type:String
    }
})
module.exports = mongoose.model("Refresh Tokens",userSchema);