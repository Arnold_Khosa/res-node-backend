const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const roomSchema = new Schema({
    roomNumber: {
        type: String
    },
    roomKey: {
        type: String
    },
    roomPrice: {
        type: String
    },
    roomOwner: {
        type: String
    },
    roomDescription: {
        type: String
    },
    roomType: {
        type: String
    },
    roomStatus: {
        type: String
    }

})
module.exports =  mongoose.model("rooms",roomSchema)