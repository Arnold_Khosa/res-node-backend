const express = require("express")
const router = express.Router();

const TenantController= require("../controller/Tenant.controller")
const Tenant_controller = new TenantController()

router.get("/",(req,res)=>{Tenant_controller.getAllTenants(req,res)});
router.get("/:id",(req,res)=>{Tenant_controller.getTenantById(req,res)});
router.post("/",(req,res)=>{Tenant_controller.createTenant(req,res)});
router.patch("/:id",(req,res)=>{Tenant_controller.updateTenant(req,res)});
router.delete("/:id",(req,res)=>{Tenant_controller.deleteTenant(req,res)});
//
module.exports = router;
