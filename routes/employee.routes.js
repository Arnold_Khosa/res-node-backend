const express = require("express")
const router = express.Router();

const EmployeeController= require("../controller/employee.controller")
const Employee_controller = new EmployeeController()

router.get("/",(req,res)=>{Employee_controller.getAllEmployees(req,res)});
router.get("/:id",(req,res)=>{Employee_controller.getEmployeeById(req,res)});
router.post("/",(req,res)=>{Employee_controller.createEmployee(req,res)});
router.patch("/:id",(req,res)=>{Employee_controller.updateEmployee(req,res)});
router.delete("/:id",(req,res)=>{Employee_controller.deleteEmployee(req,res)});

module.exports = router;
