const express = require("express")
const router = express.Router();

const WaitingListController= require("../controller/waitinglist.controller")
const WaitingList_controller = new WaitingListController()

router.get("/",(req,res)=>{WaitingList_controller.getAllWaitingLists(req,res)});
router.get("/:id",(req,res)=>{WaitingList_controller.getWaitingListById(req,res)});
router.post("/",(req,res)=>{WaitingList_controller.createWaitingList(req,res)});
router.patch("/:id",(req,res)=>{WaitingList_controller.updateWaitingList(req,res)});
router.delete("/:id",(req,res)=>{WaitingList_controller.deleteWaitingList(req,res)});
//
module.exports = router;
