const express = require("express")
const router = express.Router();

const UserController= require("../controller/user.controller")
const User_controller = new UserController();

//router.get("/",(req,res)=>{User_controller.getUsers(req,res)});
router.post("/",(req,res)=>{User_controller.login(req,res)});
//router.post("/refreshtoken",(req,res)=>{User_controller.refreshToken(req,res)});
router.post("/",(req,res)=>{User_controller.addUser(req,res)});
router.patch("/:id",(req,res)=>{User_controller.EditUser(req,res)});
router.delete("/:id",(req,res)=>{User_controller.deleteUser(req,res)});
//
module.exports = router;
