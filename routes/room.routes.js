const express = require("express")
const router = express.Router();

const RoomController= require("../controller/Room.controller")
const Room_controller = new RoomController()

router.get("/",(req,res,next)=>{ Room_controller.getAllRooms(req,res)});
router.get("/:id",(req,res)=>{Room_controller.getRoomById(req,res)});
router.post("/",(req,res)=>{Room_controller.createRoom(req,res)});
router.patch("/:id",(req,res)=>{Room_controller.updateRoom(req,res)});
router.delete("/:id",(req,res)=>{Room_controller.deleteRoom(req,res)});
//
module.exports = router;
